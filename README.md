# Scrapers

### Install
```bash
$ git clone git@bitbucket.org:banderol/scrapers.git
$ cd scrapers
$ npm install
```

### Usage
```bash
$ ./bin/scrapers
```

```bash
$ npm start
```

### ESLint
Before each commit run linter and make sure that there are no error. This will allow us to keep our codebase in the same style (AirBnB).

```bash
$ npm run lint
```

- goto: http://localhost:7834/ttn/$TTN/scrape
- Where $TTN is take from here : https://goo.gl/atgC7n

### Isues
- All issues on *pivotal*

### WIKI
https://bitbucket.org/redbonesmith/scrapers/wiki/Home
