import _ from 'lodash';
import cheerio from 'cheerio';
import config from '../config/config';
import Promise from 'bluebird';
import validator from 'validator';
import request from 'request';

const requestImpl = Promise.promisify(request);

function clientError(err) {
  console.trace(err);
  return err.code >= 400 && err.code < 500;
}

function matchString(str, where) {
  const match = /(str)/;
  return match.test(where);
}

function cleanText(word) {
  if (!word) return console.error('Not a word');
  return word.trim();
}

function parseData(html) {
  let info = [];
  const $ = cheerio.load(html);

  $('div.highlight p').each(() => {
    const cleanWords = $(this).text().split(':').map(cleanText);
    info.push({
      key: cleanWords[0],
      value: cleanWords[1],
    });
    // this line needed for location finding
    return $(this).remove();
  });

  info = info.slice(0, 4);
  const probablyLocation = $('div.highlight').text().split('\n').slice(2, 3).map(cleanText);

  if (probablyLocation[0] === 'Current location:') info.push({ status: probablyLocation[1]});
  return info;
}

function ruleEstimateDeliveryDate(el) {
  return {
    estimatedDate: el.value.replace(/\./g, '-'),
  };
}

function ruleAddress(el) {
  return {
    deliveryStorageNumber: el.value.split(' ')[1].replace('№', ''),
  };
}

function ruleWarnMsg(el) {
  // if UAH in el.value => this might be a delivery cost
  if (!el.value) {
    return {
      status: {
        key: 'status',
        value: 'delivered',
      },
    };
  }

  if (el.key) return { msg: cleanText(el.key) };
  if (el.value) return { msg: cleanText(el.value) };
}

function ruleForRoute(el) {
  const towns = el.value.split('-');
  const route = {
    from: cleanText(towns[0]),
    to: cleanText(towns[1]),
  };
  return {route};
}

function ruleStatus(el) {
  // @todo: logic for parsing status
  // for now just return value
  const status = {
    key: 'status',
    value: el.status,
  };
  return {status};
}

function ruleRouter(el) {
  if (el.value) {
    if (matchString('date', el.key)) return ruleEstimateDeliveryDate(el);
    if (el.key === 'Route') return ruleForRoute(el);
    if (matchString('address', el.key)) return ruleAddress(el);
    if (el.status) return ruleStatus(el);
    return {};
  } else {
    return ruleWarnMsg(el);
  }
}


const scrapeData = Promise.method((ttn) => {
  // console.log res.req.headers, 'headers'
  requestImpl(`${config.NEWPOSTLINK}${ttn}`).spread((res, html) => {
    const data = parseData(html);
    const info = [];
    let statusCounter = 0;
    for (const el of data) {
      // BB-3 issue:
      // fixMe: this fugly - duplicate remover
      // problem: info:
      // [ { route: { from: 'Kiїv', to: 'Lʹvіv' } },
      // { status: { key: 'status', value: 'delivered' } },
      // { status: { key: 'status', value: 'delivered' } } ]
      // wrong status assigning
      if (el.status) statusCounter++;
      if (statusCounter === 0) {
        const result = ruleRouter(el);
        if (!_.isEmpty(result)) info.push(result);
      }
    }

    return info;
  }).catch(clientError);
});

function ttnValidator(_ttn) {
  const hasCorrectLen = _ttn.length === 11 || _ttn.length === 14;
  const isNumeric = validator.isNumeric(_ttn);
  return hasCorrectLen && isNumeric;
}

exports.scrape = (req, res, next) => {
  const id = req.params.id;
  if (!id) return res.status(400).json({error: 'no ttn'});

  if (ttnValidator(id)) {
    scrapeData(id, res)
    .then((rendered) => {
      const doc = {
        ttn: id,
        data: rendered,
      };

      res.setHeader('Content-Type', 'application/json');
      res.setHeader('charset', 'utf-8');

      // if no info was found
      if (doc.data === undefined) {
        doc.data = 'Cant find package data';
      }
      return res.json(doc);
    })
    .catch((err) => {
      console.error({err}, 'scrapeData error');
      return next(err);
    });
  } else {
    return res.status(400).json({error: 'Wrong TTN'});
  }
};
