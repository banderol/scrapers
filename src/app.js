import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import mongoose from 'mongoose';
import logger from 'morgan';
import config from './config/config';
import routes from './routes';

const env = process.env.NODE_ENV || 'development';

// open database connection
mongoose.set('debug', config.MONGOOSE_DEBUG);
mongoose.connect(config.MONGODB_URI);

const db = mongoose.connection;

db.on('error', (err) => {
  if (err !== null) console.error({err: err, module: 'app'});
});

const app = express();

app.set('trust proxy', ['loopback', 'linklocal', 'uniquelocal']);

app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extended: true}));

app.use(logger('dev'));
app.use(cors({origin: true}));
app.use(routes);
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

app.locals.ENV = env;
app.locals.ENV_DEVELOPMENT = env === 'development';

// error handlers

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
// will print stacktrace

if (app.get('env') === 'development') {
  app.use((err, req, res) => {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
      title: 'error',
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res) => {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
    title: 'error',
  });
});

export default app;
