import express from 'express';
import scrapingService from './services/scraping';

const router = new express.Router();

router.get('/ttn/:id/scrape', scrapingService.scrape);

module.exports = router;
