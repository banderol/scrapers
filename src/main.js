import http from 'http';
import app from './app';

import config from './config/config';
import pkg from '../package.json';

const server = http.createServer(app);

function onListen(err) {
  if (err) {
    console.error(err, `error starting ${pkg.name}`);
    process.exit(1);
  }

  const address = server.address();
  console.log(`${pkg.name} listening on http://${address.address}:${address.port}`);
}

server.once('error', (err) => {
  if (err.code === 'EADDRINUSE') return onListen(err);
  throw err;
});

if (config.HOST === '0.0.0.0' || config.HOST === '::') {
  server.listen(config.PORT, onListen);
} else {
  server.listen(config.PORT, config.HOST, onListen);
}
