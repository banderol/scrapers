import pkg from '../../package.json';
const config = {
  PORT: parseInt(process.env.PORT || '7834', 10),
  MONGODB_URI: process.env.MONGODB_URI || `mongodb://localhost/${pkg.name}`,
  MONGOOSE_AUTOINDEX: process.env.MONGOOSE_AUTOINDEX === '1',
  MONGOOSE_DEBUG: process.env.MONGOOSE_DEBUG === '1',
  REDIS_URL: process.env.REDIS_URL || 'redis://redis:secret@localhost/1',
  REDIS_PORT: process.env.REDIS_PORT || '6379',
  REDIS_CACHE_INTERVAL: parseInt(process.env.REDIS_CACHE_INTERVAL || '900', 10),
  redisErrorMessage: 'Redis server is unavailable',
  redisRetryDelay: 100,
  redisRetryMaxDelay: 5000,
  NEWPOSTLINK: 'https://novaposhta.ua/en/tracking/?cargo_number=',
};

module.exports = config;
